'use strict'

var login = angular.module("loginModule",['ngRoute','commonModule']);

login.controller('loginController',['$scope','loginService','authService',function($scope,loginService,authService){
  $scope.pageTitle = "login page";
  console.log(authService.userLoggedIn());
  loginService.doSomething();
}])
