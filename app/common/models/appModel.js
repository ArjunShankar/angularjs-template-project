/**
 * Created by arjun on 1/3/17.
 */
var appModel = angular.module('commonModule');

appModel.factory('applicationModel',[function(){
    var _config = {};

    _config.errorConsts={
        'ERROR':'An error was encountered.',
        'NODATA':'No Data Available.'
    };
    _config.sessionStatus=true;

    return _config;

}])