/**
 * Created by arjun on 1/3/17.
 */
var commonServices = angular.module('commonModule');

commonServices.factory('authService',['applicationModel',function(applicationModel){
    var _service={};

    _service.userLoggedIn = function(){
        return applicationModel.sessionStatus;
    }

    return _service;

}])