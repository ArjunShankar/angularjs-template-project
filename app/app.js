'use strict';

// Declare app level module which depends on views, and components
var app = angular.module('myApp', [
  'ngRoute',
  'dashboardModule',
  'loginModule'
]);




app.config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
  $locationProvider.hashPrefix('!');

  $routeProvider
      .when('/dashboard',{
          templateUrl: 'dashboard/templates/dashboard.html',
          controller: 'dashboardController'
      })
      .when('/login', {
          templateUrl: 'login/templates/login.html',
          controller: 'loginController'
      })
      .otherwise({redirectTo: '/dashboard'});
}]);
