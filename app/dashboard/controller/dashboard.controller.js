'use strict';

var dashboard = angular.module('dashboardModule', ['ngRoute']);

dashboard.controller('dashboardController', ['$scope','dashboardService',function($scope,dashboardService) {
  $scope.welcomeText = "dashboard";
  dashboardService.doSomething();
}]);